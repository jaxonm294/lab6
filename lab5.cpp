#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
  Card deck[52];
  for(int i = 0; i < 13; i++){
	deck[i].suit = SPADES;
	deck[i].value = i+2;
  }
  for(int j  = 0; j < 13; j++){
	deck[j+13].suit = HEARTS;
	deck[j+13].value = j+2;
  }
  for(int k = 0; k < 13; k++){
	deck[k+26].suit = DIAMONDS;
	deck[k+26].value = k+2;
  }
  for(int l = 0; l < 14; l++){
	deck[l+39].suit = CLUBS;
	deck[l+39].value = l+2;
	}
	

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  random_shuffle(&deck[0], &deck[52], myrandom);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
  Card hand[5] = {deck[0],deck[1],deck[2],deck[3],deck[4]};


    /*Sort the cards.  Links to how to call this function is in the specs
     provided*/
  //sort(&deck[0], &deck[52], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */

  for(int p = 0; p < 5; p++){
	cout<<setw(2)<<hand[p].value<<setw(3)<<"of"<<setw(2)<<hand[p].suit<<"\n";
  }


  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
}

string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
}
